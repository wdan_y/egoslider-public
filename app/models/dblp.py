# import json
from datetime import datetime
from pymongo import MongoClient
import networkx as nx

import math
import numpy as np
import matplotlib
import pylab
from sklearn import manifold


class DBLP:

    client = MongoClient('localhost', 27017)

    @classmethod
    def getBaselineData(cls, dbName, collName, nameList):
        db = cls.client[dbName]
        data = db[collName]
        queryObj = {'$or': []}
        for n in nameList:
            queryObj['$or'].append(n)
        results = data.find(queryObj)
        authorList = []
        for res in results:
            obj = {}
            obj['id'] = res['id']
            obj['name'] = res['id']
            if collName == 'enron' or collName == 'enron2':
                obj['id'] = obj['id'][:-10]
                obj['name'] = obj['name'][:-10]
            # obj['neighbors'] = res['neighbors']
            # obj['neighborLen'] = len(res['neighbors'])
            obj['neighbors'] = res['allNeighborList']
            obj['allNeighborDict'] = res['allNeighborDict']
            obj['neighborLen'] = len(res['allNeighborList'])
            obj['yearDict'] = res['yearDict']
            minYear = None
            maxYear = None
            for key, item in res['yearDict'].iteritems():
                maxYear = cls.getMax(key, maxYear)
                minYear = cls.getMin(key, minYear)
                res['yearDict']['pos'] = cls.getSpringPos(res, key)
            obj['publication'] = res['publications']
            obj['startYear'] = minYear
            obj['endYear'] = maxYear
            authorList.append(obj)
        return authorList

    @classmethod
    def getSpringPos(cls, data, year):
        nodes = data['yearDict'][str(year)]['neighborList']
        edges = data['yearDict'][str(year)]['edgeList']
        g = nx.Graph()
        edges = map(lambda a: (nodes[a['index1']], nodes[a['index2']]), edges)
        g.add_nodes_from(nodes)
        g.add_node(res['id'])
        g.add_edges_from(edges)
        for n in nodes:
            g.add_edge(res['id'], n)
        return nx.spring_layout(g)

    @classmethod
    def authorObjList(cls, dbName, collName):
        db = cls.client[dbName]
        data = db[collName]
        if collName == 'facebook':
            results = data.find({"publications": {"$gte": 200}})
        elif collName == 'enron' or collName == 'enron2':
            #results = data.find({"publications": {"$gte": 2000}})
            results = data.find()
        else:
            results = data.find({"publications": {"$gte": 20}})
        authorList = []
        for res in results:
            obj = {}
            obj['id'] = res['id']
            obj['name'] = res['id']
            if collName == 'enron' or collName == 'enron2':
                obj['id'] = obj['id'][:-10]
                obj['name'] = obj['name'][:-10]
            # obj['neighbors'] = res['neighbors']
            # obj['neighborLen'] = len(res['neighbors'])
            obj['neighbors'] = res['allNeighborList']
            obj['allNeighborDict'] = res['allNeighborDict']
            obj['neighborLen'] = len(res['allNeighborList'])
            obj['yearDict'] = res['yearDict']
            minYear = None
            maxYear = None
            for key, item in res['yearDict'].iteritems():
                maxYear = cls.getMax(key, maxYear)
                minYear = cls.getMin(key, minYear)
                #obj['yearDict'][key]['signature'] = cls.genSignature(item)
            obj['publication'] = res['publications']
            obj['startYear'] = minYear
            obj['endYear'] = maxYear
            authorList.append(obj)
        return authorList

    @classmethod
    def testDraw(cls, authorList, year):
        filteredList = []
        for i in xrange(len(authorList)):
            if str(year) in authorList[i]['yearDict']:
                filteredList.append(authorList[i])
        similarities = []
        for i in xrange(len(filteredList)):
            similarities.append([])
            for j in xrange(len(filteredList)):
                if i == j:
                    similarities[i].append(1)
                else:
                    similarities[i].append(cls.canberraDistance(filteredList[i]['yearDict'][str(year)], filteredList[j]['yearDict'][str(year)]))
        similarities = np.array(similarities)
        n = len(filteredList)
        if n==0:
            return []
        similarities = np.ndarray((n, n), buffer=similarities, dtype=float)
        mds = manifold.MDS(n_components=2, max_iter=1000, eps=1e-9,
                           dissimilarity="precomputed", n_jobs=1)
        pos = mds.fit(similarities).embedding_
        return pos

    @classmethod
    def cosineSimilarity(cls, a, b):
        numerator = 0
        denominatorA = 0
        denominatorB = 0
        for i in xrange(len(a['signature'])):
            numerator += a['signature'][i] * b['signature'][i]
            denominatorA += a['signature'][i] * a['signature'][i]
            denominatorB += b['signature'][i] * b['signature'][i]
        denominator = math.sqrt(float(denominatorA)) * math.sqrt(float(denominatorB))
        if denominator == 0:
            return 0
        else:
            return float(numerator) / denominator

    @classmethod
    def canberraDistance(cls, a, b):
        res = 0
        for i in xrange(len(a['signature'])):
            absSum = abs(a['signature'][i]) + abs(b['signature'][i])
            if absSum != 0:
                res += float(abs(a['signature'][i] - b['signature'][i])) / absSum
        return res

    @classmethod
    def genSignature(cls, itemDict):
        # 0: Number of 1-degree neighbor (alter)
        # 1: Clustering coefficient
        # 2: Number of avg 2-degree neighbor per alter
        # 3: Edge number
        # 4: Number of 2-hop edges
        # 5: Number of 2-degree neighbors
        # Optional 6: Avg alter clustering coefficient
        # Optional 7: Avg alter tie strength
        signature = []
        signature.append(len(itemDict['neighborList']))
        signature.append(itemDict['density'])
        signature.append(itemDict['avgNeighborLocalDegree'])
        signature.append(len(itemDict['edgeList']))
        signature.append(itemDict['secondDegreeEdgeCnt'])
        signature.append(len(itemDict['secondDegreeNeighborList']))
        signature.append(sum(itemDict['tieStrength'].values()) * 1.0 / len(itemDict['neighborList']))
        return signature

    @classmethod
    def getMax(cls, a, b):
        if b is None:
            return int(a)
        a = int(a)
        b = int(b)
        if a / 10000 == 0:
            return max(int(a), int(b))
        else:
            yearA = a / 100
            monthA = a % 100
            yearB = b / 100
            monthB = b % 100
            dateA = datetime(yearA, monthA, 1)
            dateB = datetime(yearB, monthB, 1)
            if dateA > dateB:
                return a
            else:
                return b

    @classmethod
    def getMin(cls, a, b):
        if b is None:
            return int(a)
        a = int(a)
        b = int(b)
        if a / 10000 == 0:
            return min(int(a), int(b))
        else:
            yearA = a / 100
            monthA = a % 100
            yearB = b / 100
            monthB = b % 100
            dateA = datetime(yearA, monthA, 1)
            dateB = datetime(yearB, monthB, 1)
            if dateA > dateB:
                return b
            else:
                return a

    @classmethod
    def authorSchema(cls):
        schemaList = []
        schemaList.append({'name': 'name', 'type': 'String', 'alias': 'Name'})
        schemaList.append({'name': 'startYear', 'type': 'Float', 'alias': 'Start'})
        schemaList.append({'name': 'endYear', 'type': 'Float', 'alias': 'End'})
        schemaList.append({'name': 'neighborLen', 'type': 'Float', 'alias': 'Degree'})
        schemaList.append({'name': 'publication', 'type': 'Float', 'alias': 'Pub'})
        return schemaList

    @classmethod
    def select(cls, authorList, date):
        res = []
        for item in authorList:
            if str(date) in item['yearDict']:
                res.append(item)
        return res

    #@classmethod
    #def getPerformance(cls, dataset, startDate, endDate):
        #dateRange = cls.genDateRange(startDate, endDate)
        #client = MongoClient('localhost', 27017)
        #if dataset == 'enron':
            #db = client["egoNetwork_enron_mail"]
            #data = db["enron"]
            #results = data.find()
        #elif dataset == 'ml' or dataset == 'vis_graphic':
            #db = client["dblp_egoNetwork_Combined"]
            #data = db[dataset]
            #results = data.find()
        #else:
            #db = client["egoNetwork_facebook"]
            #data = db["facebook"]
            #results = data.find()
        #authorDict= {}

    @classmethod
    def getOverviewData(cls, dataset, startDate, endDate):
        dateRange = cls.genDateRange(startDate, endDate)
        client = MongoClient('localhost', 27017)
        if dataset == 'enron':
            db = client["egoNetwork_enron_mail"]
            data = db["enron"]
            results = data.find()
        elif dataset == 'ml' or dataset == 'vis_graphic':
            db = client["dblp_egoNetwork_Combined"]
            data = db[dataset]
            results = data.find({"publications": {"$gte": 20}})
        else:
            db = client["egoNetwork_facebook"]
            data = db["facebook"]
            results = data.find({"publications": {"$gte": 200}})
        authorList = []
        for res in results:
            obj = {}
            obj['id'] = res['id']
            obj['name'] = res['id']
            # obj['neighbors'] = res['neighbors']
            # obj['neighborLen'] = len(res['neighbors'])
            obj['neighbors'] = res['allNeighborList']
            obj['neighborLen'] = len(res['allNeighborList'])
            obj['yearDict'] = res['yearDict']
            minYear = None
            maxYear = None
            for key, item in res['yearDict'].iteritems():
                maxYear = DBLP.getMax(key, maxYear)
                minYear = DBLP.getMin(key, minYear)
                obj['yearDict'][key]['signature'] = DBLP.genSignature(item)
            obj['publication'] = res['publications']
            obj['startYear'] = minYear
            obj['endYear'] = maxYear
            authorList.append(obj)
        return cls.calPos(authorList, dateRange)

    @classmethod
    def getOverviewData2(cls, dataset, startDate, endDate):
        client = MongoClient('localhost', 27017)
        if dataset == 'enron' or dataset == 'enron2':
            db = client["egoNetwork_enron_mail"]
        elif dataset == 'ml' or dataset == 'vis_graphic':
            db = client["dblp_egoNetwork_Combined"]
        else:
            db = client["egoNetwork_facebook"]

        collectionName = dataset+"_yearList"
        data = db[collectionName]
        results = data.find({"$and":[{"year":{"$gte": startDate}},{"year":{"$lte": endDate}}]})
        dateList =[]
        for res in results:
            date = res["year"]
            userList = res["userList"]
            year = {}
            year['key'] = str(date)
            year['value'] = {}
            for user in userList:
                newUser = {}
                newUser['id'] = user['id']
                if dataset == 'enron' or dataset == 'enron2':
                    newUser['id'] = newUser['id'][:-10]
                newUser['info'] = user['yearDict']
                newUser['x'] = user['posX']
                newUser['y'] = user['posY']
                year['value'][user['id']] = newUser
            dateList.append(year)
        return dateList


    @classmethod
    def calPos(cls, authorList, dateRange):
        res = []
        for date in dateRange:
            tempDict = {}
            key = str(date)
            tempDict['key'] = key
            tempDict['value'] = {}
            for author in authorList:
                if key in author['yearDict']:
                    authorDict = {}
                    authorDict['id'] = author['id']
                    authorDict['info'] = author['yearDict'][key]
                    authorDict['x'] = len(author['yearDict'][key]['neighborList'])
                    authorDict['y'] = author['yearDict'][key]['density']
                    tempDict['value'][author['id']] = authorDict
            res.append(tempDict)
        return res

    @classmethod
    def genDateRange(cls, startDate, endDate):
        if int(startDate) > 10000:
            startYear = int(startDate) / 100
            endYear = int(endDate) / 100
            startMon = int(startDate) % 100
            endMon = int(endDate) % 100
            if startYear == endYear:
                return range(startYear * 100 + startMon, startYear * 100 + endMon + 1)
            else:
                res = []
                res += range(startYear * 100 + startMon, startYear * 100 + 13)
                for i in xrange(startYear + 1, endYear):
                    res += range(i * 100 + 1, i * 100 + 13)
                res += range(endYear * 100 + 1, endYear * 100 + endMon + 1)
                return res
        else:
            return range(int(startDate), int(endDate) + 1)

if __name__ == "__main__":
    client = MongoClient('localhost', 27017)
    db = client["dblp_egoNetwork_Combined"]
    data = db["ml"]
    results = data.find({"publications": {"$gte": 20}})

    #db = client["egoNetwork_enron_mail"]
    #data = db["enron"]
    #results = data.find()

    #db = client["egoNetwork_facebook"]
    #data = db["facebook"]
    #results = data.find({"publications": {"$gte": 200}})

    authorList = []
    for res in results:
        obj = {}
        obj['id'] = res['id']
        obj['name'] = res['id']
        # obj['neighbors'] = res['neighbors']
        # obj['neighborLen'] = len(res['neighbors'])
        obj['neighbors'] = res['allNeighborList']
        obj['neighborLen'] = len(res['allNeighborList'])
        obj['yearDict'] = res['yearDict']
        minYear = None
        maxYear = None
        for key, item in res['yearDict'].iteritems():
            maxYear = DBLP.getMax(key, maxYear)
            minYear = DBLP.getMin(key, minYear)
            obj['yearDict'][key]['signature'] = DBLP.genSignature(item)
        obj['publication'] = res['publications']
        obj['startYear'] = minYear
        obj['endYear'] = maxYear
        authorList.append(obj)
    #for i in xrange(2010, 2014):
    for i in xrange(1, 30):
        #for enron and fb dataset
        #pos = DBLP.testDraw(authorList, 2008*100 + i)
        year = 1964 + i
        print year
        pos = DBLP.testDraw(authorList, year)
        if len(pos)==0: continue
        X = map(lambda e: e[0], pos)
        Y = map(lambda e: e[1], pos)
        #date = i
        #date = 2001 * 100 + i
        #date = 2008 * 100 + i
        #tempList = DBLP.select(authorList, date)
        #colorDict = {}
        #for e in tempList:
            #x = len(e['yearDict'][str(date)]['neighborList'])
            #y = e['yearDict'][str(date)]['density']
            #if (x, y) not in colorDict:
                #colorDict[(x, y)] = 0
            #colorDict[(x, y)] += 1
        #X = map(lambda e: len(e['yearDict'][str(date)]['neighborList']), tempList)
        #Y = map(lambda e: e['yearDict'][str(date)]['density'], tempList)
        #C = map(lambda e:
                #colorDict[(len(e['yearDict'][str(date)]['neighborList']),
                    #e['yearDict'][str(date)]['density'])], tempList)
        #maxColor = max(C)
        #color = [str(1. - item * 1. / maxColor) for item in C]
        #matplotlib.pyplot.scatter(X, Y, c=color, cmap=matplotlib.cm.gray)
        matplotlib.pyplot.scatter(X, Y)
        matplotlib.pyplot.show()
