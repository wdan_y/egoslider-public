from pymongo import MongoClient
import networkx as nx
import matplotlib.pyplot as plt

if __name__ == '__main__':
    client = MongoClient('localhost', 27017)
    db = client['dblp_egoNetwork_Combined']
    collection = db['vis_graphic']
    results = collection.find({"$or": [{"id": "Ming Yang"}, {"id": "Bongshin Lee"}]})
    for res in results:
        nodes = res['yearDict']['2005']['neighborList']
        edges = res['yearDict']['2005']['edgeList']
        #print nodes
        g = nx.Graph()
        edges = map(lambda a: (nodes[a['index1']], nodes[a['index2']]), edges)
        g.add_nodes_from(nodes)
        g.add_node(res['id'])
        g.add_edges_from(edges)
        for n in nodes:
            g.add_edge(res['id'], n)
        pos = nx.spring_layout(g)
        print pos
        nx.draw(g, pos=nx.spring_layout(g))
        plt.show()
