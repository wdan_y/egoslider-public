# -*- coding: utf-8 -*-
from pymongo import MongoClient


class Record():

    client = MongoClient('localhost', 27017)
    db = client['vis15yhus']

    @classmethod
    def saveTask1(cls, element):
        task1_collection = cls.db['task1']
        task1_collection.insert(element)

    @classmethod
    def saveTask2(cls, element):
        task2_collection = cls.db['task2']
        task2_collection.insert(element)
