import networkx as nx
from pymongo import MongoClient


def getSpringPos(data, year):
    nodes = data['yearDict'][str(year)]['neighborList']
    edges = data['yearDict'][str(year)]['edgeList']
    g = nx.Graph()
    edges = map(lambda a: (nodes[a['index1']], nodes[a['index2']]), edges)
    g.add_nodes_from(nodes)
    g.add_node(res['id'])
    g.add_edges_from(edges)
    for n in nodes:
        g.add_edge(res['id'], n)
    return nx.spring_layout(g)


def genDateRange(startDate, endDate):
    if int(startDate) > 10000:
        startYear = int(startDate) / 100
        endYear = int(endDate) / 100
        startMon = int(startDate) % 100
        endMon = int(endDate) % 100
        if startYear == endYear:
            return range(startYear * 100 + startMon, startYear * 100 + endMon + 1)
        else:
            res = []
            res += range(startYear * 100 + startMon, startYear * 100 + 13)
            for i in xrange(startYear + 1, endYear):
                res += range(i * 100 + 1, i * 100 + 13)
            res += range(endYear * 100 + 1, endYear * 100 + endMon + 1)
            return res
    else:
        return range(int(startDate), int(endDate) + 1)
if __name__ == '__main__':
    client = MongoClient('localhost', 27017)
    db = client['dblp_egoNetwork_Combined']
    collection = db['vis_graphic']
    results = collection.find({"publications": {"$gte": 20}})
    numRes = 0
    for res in results:
        numRes += 1
        if numRes % 100 == 0:
            print numRes
        # yearList = res['yearDict'].keys()
        # yearList.sort(lambda a, b: int(a) - int(b))
        # cnt = 0
        # for year in yearList:
            # cnt += res['yearDict'][year]['publication']
        for key in res['yearDict']:
            pos = getSpringPos(res, key)
            temp = {}
            for author in pos:
                temp[author] = [pos[author][0], pos[author][1]]
            collection.update({'_id': res['_id']}, {'$set': {'yearDict' + '.' + key + '.' + 'pos': temp}})
