import json

from flask import request
from flask import render_template
from flask import make_response

from app import app
from app.models.dblp import DBLP
from app.models.record import Record

user_dict = {}
record = True

@app.route('/')
def root():
    return render_template('index.html')

@app.route('/userstudy')
def uesrstudy():
    return render_template('userstudy_name.html')

@app.route("/task2")
def task2():
    return app.send_static_file("views/task2_exercise.html")

@app.route("/task2test")
def task2test():
    return app.send_static_file("views/task2_test.html")

@app.route("/task2record", methods=["POST"])
def task2_record():
    username = request.cookies.get("username")
    user_dict[username]['task2'].append(request.form)
    print user_dict[username]['task2']
    return app.send_static_file("views/task2_test.html")

@app.route("/task1")
def task1():
    return app.send_static_file("views/task1_exercise.html")

@app.route("/task1test")
def task1test():
    return app.send_static_file("views/task1_test.html")

@app.route("/task1record", methods=["POST"])
def task1_record():
    username = request.cookies.get("username")
    user_dict[username]['task1'].append(request.form)
    print user_dict[username]['task1']
    return app.send_static_file("views/task1_test.html")

@app.route("/task1finish")
def task1_finish():
    resp = make_response(render_template("index.html"))
    username = request.cookies.get("username")
    user_dict[username]['finishTask1'] = True
    if record:
        Record.saveTask1(user_dict[username])
    return resp

@app.route("/task2finish")
def task2_finish():
    resp = make_response(render_template("index.html"))
    username = request.cookies.get("username")
    user_dict[username]['finishTask2'] = True
    if record:
        Record.saveTask2(user_dict[username])
    return resp

@app.route('/db_list')
def getDBList():
    dbList = ["vis_graphic", "enron"]
    #dbList = ["vis_graphic", "ml", "facebook"]
    return json.dumps(dbList)


@app.route('/node_obj_list')
def getNodeObjList():
    collName = request.args['name']
    if collName == "facebook":
        dbName = "egoNetwork_facebook"
    elif collName == "enron" or collName == "enron2":
        dbName = "egoNetwork_enron_mail"
    else:
        dbName = "dblp_egoNetwork_Combined"
    return json.dumps(DBLP.authorObjList(dbName, collName))


@app.route('/node_schema')
def getNodeSchema():
    # dbName = request.args['name']
    print "schema"
    return json.dumps(DBLP.authorSchema())

@app.route('/overview_data2', methods=["POST"])
def getOverviewData2():
    dataset = request.form["dataset"]
    startDate = request.form["startDate"]
    endDate = request.form["endDate"]
    res = DBLP.getOverviewData2(dataset, startDate, endDate)
    return json.dumps(res)

    #dataset = request.form['dataset']
    #res = DBLP.getOverviewData2(dataset)
    #return json.dumps(res);

@app.route('/overview_data', methods=["POST"])
def getOverviewData():
    dataset = request.form["dataset"]
    startDate = request.form["startDate"]
    endDate = request.form["endDate"]
    res = DBLP.getOverviewData(dataset, startDate, endDate)
    return json.dumps(res)

@app.route("/userstudy_main", methods=["GET", "POST"])
def userstudyMain():
    if (request.method == "POST"):
        username = request.form["username"]
        user_dict[username] = {}
        user_dict[username]['name'] = username
        user_dict[username]['task1'] = []
        user_dict[username]['task2'] = []
        user_dict[username]['finishTask1'] = False
        user_dict[username]['finishTask2'] = False
        resp = make_response(render_template("userstudy_main.html", name=username, f1=user_dict[username]['finishTask1'], f2=user_dict[username]['finishTask2']))
        resp.set_cookie('username', username)
        return resp
    else:
        username = request.cookies.get("username")
        finishTask1 = user_dict[username]['finishTask1']
        finishTask2 = user_dict[username]['finishTask2']
        if finishTask1 and finishTask2:
            return render_template("thanks.html")
        resp = make_response(render_template("userstudy_main.html", name=username, f1=finishTask1, f2=finishTask2))
        return resp
